﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReportes : Form
    {


        private DataSet dsSistema;
        private BindingSource bsReportes;
        public FrmReportes()
        {
            InitializeComponent();
            bsReportes = new BindingSource();
        }

        public DataSet DSReportes { set{ dsSistema = value; } }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmReporteFactura frmRF = new FrmReporteFactura();
            frmRF.Show();
        }

        private void FrmReportes_Load(object sender, EventArgs e)
        {
            bsReportes.DataSource = dsSistema;
            bsReportes.DataMember = dsSistema.Tables["ReporteFactura"].TableName;
            dgvReporte.DataSource = bsReportes;
            dgvReporte.AutoGenerateColumns = true;
        }

        private void txtFinderReport_TextChanged(object sender, EventArgs e)
        {
            try
            {
               bsReportes.Filter = string.Format("Sku like '*{0}*' or Cod_Factura like '*{0}*' or Fecha like '*{0}*' or Nombre_Empleado like '*{0}*' or Nombre_Cliente like '*{0}*'", txtFinderReport.Text);

            }catch(InvalidExpressionException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private void btnVerReport_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection rowCollectiones = dgvReporte.SelectedRows;

            if(rowCollectiones.Count == 0)
            {
                MessageBox.Show(this, "Error debe selecionar una fila", "Mensaje  de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            FrmReporteFactura reporteFac = new FrmReporteFactura();
            reporteFac.Dssitema = dsSistema;
            reporteFac.Show();
        }
    }
}
