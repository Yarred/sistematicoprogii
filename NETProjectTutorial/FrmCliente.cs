﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drCliente;

        public FrmCliente()
        {
            InitializeComponent();
        }

        public DataTable TblClientes
        {
            set
            {
                tblClientes = value;
            }
        }

        public DataSet DsClientes
        {
            set
            {
                dsClientes = value;
            }
        }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;

                txtNombres.Text = drCliente["Nombre"].ToString();
                txtApellidos.Text = drCliente["Apellido"].ToString();
                mskCedula.Text = drCliente["Cedula"].ToString();
                mskTelefono.Text = drCliente["Telefono"].ToString();
                txtCorreo.Text = drCliente["Correo"].ToString();
                txtDireccion.Text = drCliente["Direccion"].ToString();
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            string nombres, apellidos;
            string cedula, telefono;
            string correo, direccion;

            try
            {
                nombres = txtNombres.Text;
                apellidos = txtApellidos.Text;
                cedula = mskCedula.Text;
                telefono = mskTelefono.Text;
                correo = txtCorreo.Text;
                direccion = txtDireccion.Text;

                if (drCliente != null)
                {
                    DataRow drNew = tblClientes.NewRow();

                    int index = tblClientes.Rows.IndexOf(drCliente);
                    drNew["Id"] = drCliente["Id"];
                    drNew["Nombre"] = nombres;
                    drNew["Apellido"] = apellidos;
                    drNew["Cedula"] = cedula;
                    drNew["Telefono"] = telefono;
                    drNew["Correo"] = correo;
                    drNew["Direccion"] = direccion;

                    tblClientes.Rows.RemoveAt(index);
                    tblClientes.Rows.InsertAt(drNew, index);

                }
                else
                {
                    tblClientes.Rows.Add(tblClientes.Rows.Count + 1, nombres, apellidos, cedula, telefono, correo, direccion);
                }

                Dispose();
            }
            catch (FormatException)
            {
                MessageBox.Show(this, "Error, Campos vacios", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

    }
}
