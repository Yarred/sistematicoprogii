﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestioCliente : Form
    {
        private DataSet dsClientes;

        public FrmGestioCliente()
        {
            InitializeComponent();
        }
        
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            FrmCliente fc = new FrmCliente();
            fc.TblClientes = dsClientes.Tables["Cliente"];
            fc.DsClientes = dsClientes;
            fc.ShowDialog();

        }
    }
}
